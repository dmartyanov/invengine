package ru.tcsbank.engine.game.logic

import org.scalamock.scalatest.MockFactory
import ru.tcsbank.BinvTest
import ru.tcsbank.bot.BotMove
import ru.tcsbank.engine.game.planets.{Arzakena, Planet}
import ru.tcsbank.tags.Unit

/**
 * Created by d.a.martyanov on 12.12.14.
 */
@Unit
class GalaxyProcessorTest extends BinvTest with MockFactory {

  test("galaxy processor should sort armies") {
    GalaxyProcessor.sortArmies(
      List(
        (Option("a"), 15), (Option("b"), 10), (None, 35)
      )
    ) shouldBe (
      List(
        (None, 35), (Option("a"), 15), (Option("b"), 10)
      )
      )

    GalaxyProcessor.sortArmies(
      List((Option("a"), 15), (Option("b"), 15))
    ) shouldBe (
      List((Option("a"), 15), (Option("b"), 15))
      )
  }

  test("galaxy processor should count buttle result") {
    val planet = Planet("a", 10, None, List(), Arzakena)
    GalaxyProcessor.planetButtleResult(planet, List(
      BotMove("b", "a", 10, None)
    )) shouldBe Planet("a", 20, None, List(), Arzakena)

    GalaxyProcessor.planetButtleResult(planet, List(
      BotMove("b", "a", 9, Some("bot1"))
    )) shouldBe Planet("a", 1, None, List(), Arzakena)

    GalaxyProcessor.planetButtleResult(planet, List(
      BotMove("b", "a", 11, Some("bot1"))
    )) shouldBe Planet("a", 1, Some("bot1"), List(), Arzakena)

    GalaxyProcessor.planetButtleResult(planet, List(
      BotMove("b", "a", 15, Some("bot1")),
      BotMove("c", "a", 10, None)
    )) shouldBe Planet("a", 5, None, List(), Arzakena)

    GalaxyProcessor.planetButtleResult(planet, List(
      BotMove("b", "a", 15, Some("bot1")),
      BotMove("c", "a", 10, None),
      BotMove("d", "a", 30, Some("bot2"))
    )) shouldBe Planet("a", 10, Some("bot2"), List(), Arzakena)
  }

  test("galaxy processor should update bot amount planet") {
    val planet = Planet("a", 10, None, List(), Arzakena)
    GalaxyProcessor.updatePlanetBotAmount(50)(planet) shouldBe Planet("a", 50, None, List(), Arzakena)
    GalaxyProcessor.updatePlanetBotAmount(150)(planet) shouldBe Planet("a", 100, None, List(), Arzakena)
    GalaxyProcessor.updatePlanetBotAmount(-10)(planet) shouldBe Planet("a", 0, None, List(), Arzakena)
  }

  test("galaxy processor should count evolution planet") {
    val planet = Planet("a", 10, None, List(), Arzakena)
    val planetOwned = Planet("a", 10, Some("b1"), List(), Arzakena)
    GalaxyProcessor.evolutionPlanet(planet) shouldBe (Planet("a", 30, None, List(), Arzakena))
    GalaxyProcessor.evolutionPlanet(planetOwned) shouldBe (Planet("a", 31, Some("b1"), List(), Arzakena))
  }

  test("galaxy processor should define game winner"){
    GalaxyProcessor.winnerId(Map("10" -> 10, "12" -> 12, "3" -> 3)) shouldBe "12"
    GalaxyProcessor.winnerId(Map("10" -> 10, "3" -> 3, "3" -> 3)) shouldBe "10"
    GalaxyProcessor.winnerId(Map("10" -> 10)) shouldBe "10"
  }

}
