package ru.tcsbank.connection

import akka.actor.{Actor, Props}
import akka.event.LoggingReceive
import ru.tcsbank.dao.reactive.{AddConnection, AppendUser, EngineDaoWorker}
import ru.tcsbank.dao.schema.ConnectionsDao
import ru.tcsbank.engine.ladder.FindLadderGame
import ru.tcsbank.system.exception.IllegalRequestException
import ru.tcsbank.system.{Global, Logger}
import ru.tcsbank.utils.config.ConfigHelper
import ru.tcsbank.utils.context.ExecutionContextComponent
import ru.tcsbank.utils.json.JsonHelper
import ru.tcsbank.utils.logging.LoggerHelper

import scala.util.{Failure, Success, Try}

/**
 * Created by d.a.martyanov on 08.12.14.
 */
class EngineConnectionManager extends Actor with UserCreator {
  this: ConfigHelper with JsonHelper with LoggerHelper with ExecutionContextComponent =>

  override val lf = Logger[EngineConnectionManager]
  var connected = scala.collection.mutable.Map[String, ClientConnection]()

  lazy val host = config.get[String]("akka.remote.netty.tcp.host", "127.0.0.1")
  lazy val port = config.get[Int]("akka.remote.netty.tcp.port", 9898)
  lazy val hostString = s"akka.tcp://${context.system.name}@$host:$port${context.self.path.toStringWithoutAddress}"

  val ladderManager = context.actorOf(Props(Global.ladderManager), "ladder")
  val daoWorker = context.actorOf(Props(new EngineDaoWorker with LoggerHelper))

  override def receive: Receive = LoggingReceive {
    case Connect(id, path) =>
      daoWorker ! AppendUser(newUser(id))
      sender ! responseSafe(appendClient(id, path))
    case Disconnect(id, ticket) => disconnectClient(id, ticket)
    // sender ! responseSafe(disconnectClient(id, ticket))
    case FindGame(id, ticket, diff) if connected.get(id).map(_.ticket) == Some(ticket) =>
      val s = sender()
      ladderManager ! FindLadderGame(id, ticket, s)
      s ! "Wait for arena creation"
    case FindGame(id, ticket, diff) =>
      sender ! ResponseFormer.error(loggedException(unknownClientException(id, ticket), WARN))
  }

  def appendClient(id: String, path: String): Try[ConnectionResult] = Try {
    connected.get(id) match {
      case Some(con) if new java.util.Date().getTime - con.dateTime < 10000 =>
        loggedException(
          new IllegalRequestException(
            s"Request[connectClient] failed: client [$id] is allready connected "
          ), WARN
        )
      case _ => ConnectClient(id, path)
    }
  } map { c =>
    connected.put(id, c)
    daoWorker ! AddConnection(c)
    ConnectionResult(connectionStatus = "connected", ticket = Some(c.ticket), host = hostString)
  }

  def responseSafe[T <: AnyRef](resp: Try[T]) = resp match {
    case Success(result) => result
    case Failure(err) => ResponseFormer.error(err)
  }

  def disconnectClient(id: String, ticket: String): Unit = Try {
    connected.get(id) match {
      case Some(cc) if cc.ticket == ticket =>
        connected.remove(id)
        ConnectionsDao.close(ticket)
      case _ => loggedException(unknownClientException(id, ticket), WARN)
    }
  }

  def unknownClientException(id: String, ticket: String) =
    new IllegalRequestException(
      s"Request[disconnectClient] failed: there is no client with requisites: [$id] @ [$ticket]"
    )

  @scala.throws[Exception](classOf[Exception])
  override def preStart() =
    log(s"Engine started on address [$hostString]", INFO)

  @scala.throws[Exception](classOf[Exception])
  override def postStop = {
    log(s"Engine terminated on address [$hostString]", INFO)
    context.system.shutdown()
  }
}
