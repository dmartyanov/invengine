package ru.tcsbank.connection

import ru.tcsbank.dao.model.User

/**
 * Created by d.a.martyanov on 23.12.14.
 */
trait UserCreator {

  val basicRating = 1000

  def newUser(id: String) = User(id, None, basicRating)
}
