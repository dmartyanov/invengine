package ru.tcsbank.connection

import ru.tcsbank.bot.BotAction

/**
 * Created by d.a.martyanov on 15.12.14.
 */
case class Connect(id: String, path: String)

case class Disconnect(id: String, ticket: String)

case class FindGame(id: String, ticket: String, diff: Int)

case class ConnectionResult(
                             connectionStatus: String,
                             ticket: Option[String] = None,
                             host: String
                             )

case class BeReady(gameId: String, rivals: List[String])

case class NextTurn(gameId: String, round: Int, galaxy: String)

case class PlayerAction(id: String, ticket: String, roundNo: Int, action: BotAction)

case class Error(desc: String)

case class YouLose(botNo: Int)

case class YouWin(botNo: Int)

case class GameResult(
                       gameId: String,
                       galaxy: String,
                       playersResult: Map[String, Int],
                       rounds: Int,
                       winner: String
                       )