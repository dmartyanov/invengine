package ru.tcsbank.connection

/**
 * Created by d.a.martyanov on 08.12.14.
 */
object ResponseFormer {
  def errMsg(errMessage: String) = Error(errMessage)
  def error(err: Throwable) = Error(err.getMessage)
}
