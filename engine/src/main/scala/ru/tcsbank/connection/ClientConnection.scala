package ru.tcsbank.connection

import java.util.Date

/**
 * Created by d.a.martyanov on 08.12.14.
 */
case class ClientConnection(
                             id: String,
                             ticket: String,
                             addrr: String,
                             dateTime: Long
                             )

object ConnectClient extends TicketGenerator{
  def apply(id: String, from: String) =
    ClientConnection(
    id = id,
    ticket = genTicket(id),
    addrr = from,
    dateTime = new Date().getTime
    )
}