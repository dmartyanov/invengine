package ru.tcsbank.connection

import ru.tcsbank.system.IdGenerator

/**
 * Created by d.a.martyanov on 08.12.14.
 */
trait TicketGenerator extends IdGenerator {
  override val constantPrefix = ""
  def genTicket = (id: String) => customPrefixId(id.take(6))
}
