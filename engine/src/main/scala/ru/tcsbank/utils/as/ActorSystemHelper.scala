package ru.tcsbank.utils.as

import akka.actor.ActorSystem

/**
 * Created by d.a.martyanov on 08.12.14.
 */
trait ActorSystemHelper {
 implicit def actorSystem: ActorSystem
}
