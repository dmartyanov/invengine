package ru.tcsbank.utils.json

import org.json4s.Extraction._
import org.json4s._
import org.json4s.reflect.TypeInfo
import ru.tcsbank.engine.game.planets._

import scala.util.{Failure, Success, Try}

/**
 * Created by d.a.martyanov on 16.12.14.
 */
class PlanetSerializer extends Serializer[Planet] {

  private val PlanetClass = classOf[Planet]

  override def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), Planet] = {
    case (TypeInfo(PlanetClass, _), jsonAst) => Try {
      Planet(
        name = (jsonAst \ "name").extract[String],
        botAmount = (jsonAst \ "botAmount").extract[Int],
        owner = Try((jsonAst \ "owner").extract[String]).toOption,
        neighbors = (jsonAst \ "neighbors").extract[List[String]],
        planetType = (jsonAst \ "pType").extract[Int] match {
          case 1 => Arzakena
          case 2 => Stintino
          case 4 => Cagliari
          case _ => Badesi
        }
      )
    } match {
      case Success(result) => result
      case Failure(exception: Exception) =>
        throw new MappingException(s"Can't convert $jsonAst to VTB24TemplateElement", exception)
      case Failure(throwable) => throw throwable
    }
  }

  override def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
    case pl: Planet =>
      decompose(Map[String, Any](
        "name" -> pl.name,
        "botAmount" -> pl.botAmount,
        "owner" -> pl.owner,
        "neighbors" -> pl.neighbors,
        "pType" -> pl.planetType.typeId
      ))
  }
}
