package ru.tcsbank.utils.json

import org.json4s.jackson.Serialization._
import org.json4s.jackson._
import org.json4s.{DefaultFormats, Formats}

/**
 * Created by d.a.martyanov on 08.12.14.
 */
trait JsonHelper {

  implicit val formats: Formats = DefaultFormats + new PlanetSerializer

  def parseJS = (jsStr: String) => parseJson(jsStr)

  def produceJS = (obj: AnyRef) => write(obj)

  def pretty = (obj: AnyRef) => writePretty(obj)
}
