import ru.tcsbank.engine.ladder.EloRatingCalculus

val elo = new EloRatingCalculus {}

(-500 to 500 by 50) map (d => elo.diffRating(d))

