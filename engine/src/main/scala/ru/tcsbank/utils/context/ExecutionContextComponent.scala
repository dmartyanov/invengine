package ru.tcsbank.utils.context

import scala.concurrent.ExecutionContext

/**
 * Created by d.a.martyanov on 08.12.14.
 */
trait ExecutionContextComponent {
  implicit def executionContext: ExecutionContext
}
