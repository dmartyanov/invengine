package ru.tcsbank.utils.config

import com.typesafe.config.Config
/**
 * Created by d.a.martyanov on 08.12.14.
 */
trait ConfigHelper {
  val config: Config
  implicit def extendedConfig(c: Config) = new ExtendedConfig(c)
}
