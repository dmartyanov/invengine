package ru.tcsbank.utils.config

import java.util.concurrent.TimeUnit

import com.typesafe.config.Config

import scala.concurrent.duration.FiniteDuration
import scala.language.implicitConversions
import scala.reflect.runtime.universe._
import scala.collection.JavaConversions._
import scala.concurrent.duration._

/**
 * Created by d.a.martyanov on 08.12.14.
 */
class ExtendedConfig(config: Config) {
  private val StringTag = typeTag[String]
  private val ScalaDurationTag = typeTag[scala.concurrent.duration.Duration]
  private val JavaDurationTag = typeTag[java.time.Duration]
  private val FiniteDurationTag = typeTag[FiniteDuration]
  private val StringListTag = typeTag[List[String]]
  private val BooleanTag = typeTag[Boolean]

  def getOpt[T](path: String)(implicit tag: TypeTag[T]):Option[T] = if (config.hasPath(path)) (tag match {
    case StringTag => Some(config.getString(path))
    case TypeTag.Int => Some(config.getInt(path))
    case TypeTag.Float => Some(config.getDouble(path).toFloat)
    case ScalaDurationTag => Some((config.getDuration(path, TimeUnit.MILLISECONDS)).millis)
    case JavaDurationTag => Some(java.time.Duration.ofMillis(config.getDuration(path, TimeUnit.MILLISECONDS)))
    case FiniteDurationTag => Some((config.getDuration(path, TimeUnit.MILLISECONDS)).millis)
    case StringListTag => Some(config.getStringList(path).toList)
    case BooleanTag => Some(config.getBoolean(path))
    case _ => throw new IllegalArgumentException(s"Configuration option type $tag not implemented")
  }).asInstanceOf[Option[T]] else None

  def get[T](path: String, default: =>T)(implicit tag: TypeTag[T]) = getOpt(path).getOrElse(default)
  def get[T](path: String)(implicit tag: TypeTag[T]) = getOpt(path).getOrElse(throw new RuntimeException(s"Configuration value at $path not found"))
}
