package ru.tcsbank.bot

/**
 * Created by d.a.martyanov on 11.12.14.
 */
case class BotAction(
                      playerId: String,
                      moves: List[BotMove] = List()
                      )

case class BotMove(
                    from: String,
                    to: String,
                    amount: Int,
                    botId: Option[String] = None
                    )
