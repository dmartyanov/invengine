package ru.tcsbank.engine.game.logic

import ru.tcsbank.bot.{BotAction, BotMove}
import ru.tcsbank.engine.game.galaxy._
import ru.tcsbank.engine.game.planets.Planet
import ru.tcsbank.system.exception.GalaxyProcessingException

/**
 * Created by d.a.martyanov on 11.12.14.
 */
object GalaxyProcessor {

  def revolution(gal: Galaxy, actions: List[BotAction]) = {
    def allowedAction(action: BotAction): Boolean = gal.players.get(action.playerId) match {
      case Some(us) if us > 0 => action.moves forall allowedMove(action.playerId)
      case _ => false
    }

    def validMoves(moves: List[BotMove]): Boolean = enoughBots(moves) && (moves forall correctLinks)

    def correctLinks(move: BotMove): Boolean =
      gal.planets.find(_.name == move.from).map(_.neighbors.contains(move.to)) == Some(true)

    def enoughBots(moves: List[BotMove]): Boolean = moves.groupBy(_.from)
      .map { case (k, ml) => k -> ml.foldLeft(0)(_ + _.amount)}
      .forall { case (name, amount) => enoughBotsOnPlanet(name, amount)}

    def allowedMove(playerId: String)(move: BotMove): Boolean =
      gal.planets.find(_.name == move.from).flatMap(_.owner) == Some(playerId)


    def enoughBotsOnPlanet(name: String, no: Int): Boolean = true

    val allMoves = actions flatMap {
      case action: BotAction if allowedAction(action) && validMoves(action.moves)  =>
          action.moves.map(_.copy(botId = Some(action.playerId)))
      case _ => List()
    }

    val newGalaxy = (botsRelocation(allMoves)
      andThen battlesForPlanets(allMoves)
//      andThen updatePLayersInfo
//      andThen updateWinnerByGalaxyState
      )(gal)
    (newGalaxy, allMoves)
  }

  def botsRelocation(moves: List[BotMove]) = (g: Galaxy) => g.copy(
    planets = g.planets map {
      pl => pl.copy(botAmount = pl.botAmount - moves.filter(_.from == pl.name).map(_.amount).sum)
    }
  )

  def battlesForPlanets(moves: List[BotMove]) = (g: Galaxy) => g.copy(
    planets = g.planets map { pl => planetButtleResult(pl, moves.filter(_.to == pl.name))}
  )

  def planetButtleResult(pl: Planet, ms: List[BotMove]): Planet = {
    val botGroups = (ms map { bm => bm.botId -> bm.amount}) :+ (pl.owner -> pl.botAmount)
    val armiesSorted = sortArmies(
      botGroups.groupBy(_._1).mapValues(_.map(_._2).sum) toList)

    armiesSorted match {
      case Nil => throw new GalaxyProcessingException("Planet error")
      case List(singleArmy) => updatePlanetBotAmount(singleArmy._2)(pl.copy(owner = singleArmy._1))
      case a :: as => updatePlanetBotAmount(a._2 - as.head._2)(pl.copy(owner = a._1))
    }
  }

  def sortArmies(as: List[(Option[String], Int)]) = as.sortWith(_._2 > _._2)

  def updatePLayersInfo = (g: Galaxy) => g.copy(
    players = g.players map {
      case (p, us) =>
        g.planets.filter(_.owner == Some(p)) match {
          case Nil => p -> 0
          case List(pl@_*) => p -> pl.map(_.botAmount).sum
        }
    }
  )

  def evolution(gal: Galaxy) = updatePLayersInfo(gal.copy(planets = gal.planets map evolutionPlanet))

  def evolutionPlanet = (pl: Planet) =>
    updatePlanetBotAmount(pl.planetType.newAmount(pl.owner.isDefined)(pl.botAmount))(pl)

  def updatePlanetBotAmount(newAmount: Int) = (pl: Planet) =>
    pl.copy(botAmount = Math.max(Math.min(newAmount, pl.planetType.limit), 0))

  def winnerId(pl: Map[String, Int]) = pl.toList.sortBy(-_._2).map(_._1).head
}
