package ru.tcsbank.engine.game.galaxy

import ru.tcsbank.engine.game.planets._

/**
 * Created by d.a.martyanov on 11.12.14.
 */
object GalaxyGenerator {
  def apply(playerIds: List[String], order: Int) =
    order % 2 match {
      case 1 => galaxy2(playerIds)
      case _ => galaxy1(playerIds)
    }

  private def galaxy1(pIds: List[String]) = {
    val p1 = Planet(name = "h1", botAmount =  0, owner = pIds.headOption, planetType = Arzakena)
    val p2 = Planet(name = "h2", botAmount =  0, owner = pIds.drop(2).headOption, planetType = Arzakena)
    val p3 = Planet(name = "h3", botAmount =  0, owner = pIds.drop(1).headOption, planetType = Arzakena)
    val p4 = Planet(name = "h4", botAmount =  0, owner = pIds.drop(3).headOption, planetType = Arzakena)
    val p5 = Planet(name = "p1", botAmount =  0, planetType = Stintino)
    val p6 = Planet(name = "p2", botAmount =  0, planetType = Stintino)
    val p7 = Planet(name = "p3", botAmount =  0, planetType = Stintino)
    val p8 = Planet(name = "p4", botAmount =  0, planetType = Stintino)
    val p9 = Planet(name = "p5", botAmount =  0, planetType = Stintino)
    val p10 = Planet(name = "p6", botAmount =  0, planetType = Stintino)
    val p11 = Planet(name = "p7", botAmount =  0, planetType = Stintino)
    val p12 = Planet(name = "p8", botAmount =  0, planetType = Stintino)
    val p13 = Planet(name = "c1", botAmount =  0, planetType = Badesi)
    val p14 = Planet(name = "c2", botAmount =  0, planetType = Badesi)

    Galaxy(
      planets = List(
        p1.copy(neighbors = List(p5.name)),
        p2.copy(neighbors = List(p7.name)),
        p3.copy(neighbors = List(p9.name)),
        p4.copy(neighbors = List(p11.name)),
        p5.copy(neighbors = List(p1.name, p6.name, p12.name, p13.name)),
        p6.copy(neighbors = List(p5.name, p7.name)),
        p7.copy(neighbors = List(p2.name, p6.name, p8.name, p13.name)),
        p8.copy(neighbors = List(p7.name, p9.name)),
        p9.copy(neighbors = List(p3.name, p8.name, p10.name, p14.name)),
        p10.copy(neighbors = List(p9.name, p11.name)),
        p11.copy(neighbors = List(p4.name, p10.name, p12.name, p14.name)),
        p12.copy(neighbors = List(p5.name, p11.name)),
        p13.copy(neighbors = List(p5.name, p7.name, p14.name)),
        p14.copy(neighbors = List(p9.name, p11.name, p13.name))
      ),
      players = pIds map { pid => pid -> 0 } toMap
    )
  }

  private def galaxy2(pIds: List[String]) = {
    val p1 = Planet(name = "h1", botAmount = 0, owner = pIds.headOption, planetType = Arzakena)
    val p2 = Planet(name = "h2", botAmount =  0, owner = pIds.drop(2).headOption, planetType = Arzakena)
    val p3 = Planet(name = "h3", botAmount =  0, owner = pIds.drop(1).headOption, planetType = Arzakena)
    val p4 = Planet(name = "h4", botAmount =  0, owner = pIds.drop(3).headOption, planetType = Arzakena)
    val p5 = Planet(name = "p1", botAmount =  0, planetType = Stintino)
    val p6 = Planet(name = "p2", botAmount =  0, planetType = Stintino)
    val p7 = Planet(name = "p3", botAmount =  0, planetType = Stintino)
    val p8 = Planet(name = "p4", botAmount =  0, planetType = Stintino)
    val p9 = Planet(name = "p5", botAmount =  0, planetType = Stintino)
    val p10 = Planet(name = "p6", botAmount =  0, planetType = Stintino)
    val p11 = Planet(name = "p7", botAmount =  0, planetType = Stintino)
    val p12 = Planet(name = "p8", botAmount =  0, planetType = Stintino)
    val p13 = Planet(name = "s1", botAmount = 0, planetType = Badesi)
    val p14 = Planet(name = "s2", botAmount = 0, planetType = Badesi)
    val p15 = Planet(name = "s3", botAmount = 0, planetType = Badesi)
    val p16 = Planet(name = "s4", botAmount = 0, planetType = Badesi)
    val p17 = Planet(name = "o1", botAmount = 0, planetType = Stintino)
    val p18 = Planet(name = "o2", botAmount = 0, planetType = Stintino)
    val p19 = Planet(name = "o3", botAmount = 0, planetType = Stintino)
    val p20 = Planet(name = "o4", botAmount = 0, planetType = Stintino)
    val p21 = Planet(name = "o5", botAmount = 0, planetType = Stintino)
    val p22 = Planet(name = "o6", botAmount = 0, planetType = Stintino)
    val p23 = Planet(name = "o7", botAmount = 0, planetType = Stintino)
    val p24 = Planet(name = "o8", botAmount = 0, planetType = Stintino)
    val p25 = Planet(name = "m1", botAmount = 0, planetType = Cagliari)

    Galaxy(
      planets = List(
        p1.copy(neighbors = List(p5.name, p12.name)),
        p2.copy(neighbors = List(p6.name, p7.name)),
        p3.copy(neighbors = List(p8.name, p9.name)),
        p4.copy(neighbors = List(p10.name, p11.name)),
        p5.copy(neighbors = List(p1.name, p13.name, p17.name)),
        p6.copy(neighbors = List(p2.name, p13.name, p18.name)),
        p7.copy(neighbors = List(p2.name, p14.name, p19.name)),
        p8.copy(neighbors = List(p3.name, p14.name, p20.name)),
        p9.copy(neighbors = List(p3.name, p15.name, p21.name)),
        p10.copy(neighbors = List(p4.name, p15.name, p22.name)),
        p11.copy(neighbors = List(p4.name, p16.name, p23.name)),
        p12.copy(neighbors = List(p1.name, p16.name, p24.name)),
        p13.copy(neighbors = List(p5.name, p6.name, p25.name)),
        p14.copy(neighbors = List(p7.name, p8.name, p25.name)),
        p15.copy(neighbors = List(p9.name, p10.name, p25.name)),
        p16.copy(neighbors = List(p11.name, p12.name, p25.name)),
        p17.copy(neighbors = List(p5.name, p24.name, p25.name)),
        p18.copy(neighbors = List(p6.name, p19.name, p25.name)),
        p19.copy(neighbors = List(p7.name, p18.name, p25.name)),
        p20.copy(neighbors = List(p8.name, p21.name, p25.name)),
        p21.copy(neighbors = List(p9.name, p20.name, p25.name)),
        p22.copy(neighbors = List(p10.name, p23.name, p25.name)),
        p23.copy(neighbors = List(p11.name, p22.name, p25.name)),
        p24.copy(neighbors = List(p12.name, p17.name, p25.name)),
        p25.copy(neighbors = List(p17.name, p13.name, p18.name, p19.name, p14.name,
          p20.name, p21.name, p15.name, p22.name,  p23.name, p16.name, p24.name))
      ),
      players = pIds map { pid => pid -> 0 } toMap
    )
  }
}
