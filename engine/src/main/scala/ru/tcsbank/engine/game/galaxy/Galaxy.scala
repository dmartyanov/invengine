package ru.tcsbank.engine.game.galaxy

import ru.tcsbank.engine.game.planets.Planet

/**
 * Created by d.a.martyanov on 09.12.14.
 */
case class Galaxy(
                   planets: List[Planet],
                   players: Map[String, Int] = Map()
                   )