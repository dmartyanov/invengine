package ru.tcsbank.engine.arena

import akka.actor.{Props, ActorRef, Actor, ReceiveTimeout}
import akka.event.LoggingReceive
import ru.tcsbank.bot.{BotAction, BotMove}
import ru.tcsbank.dao.reactive.{Stop, AddLogRecord, SetInitialGalaxy, ArenaDaoWorker}
import ru.tcsbank.utils.config.ConfigHelper
import ru.tcsbank.connection._
import ru.tcsbank.engine.game.galaxy._
import ru.tcsbank.engine.game.logic.GalaxyProcessor
import ru.tcsbank.utils.json.JsonHelper
import ru.tcsbank.engine.ladder.{Round, SetGameResult, Player}
import ru.tcsbank.system.Logger
import ru.tcsbank.utils.logging.LoggerHelper

import scala.concurrent.duration._

/**
 * Created by d.a.martyanov on 09.12.14.
 */
class ArenaManager(id: String, players: List[Player], ladder: ActorRef) extends Actor {
  this: LoggerHelper with ConfigHelper with JsonHelper =>

  override val lf = Logger[ArenaManager]
  val gp = GalaxyProcessor
  val daoWorker = context.actorOf(Props(new ArenaDaoWorker with LoggerHelper))

  val roundAction = scala.collection.mutable.Map[String, BotAction]()
  val arenaHistory = scala.collection.mutable.Map[Int, List[BotMove]]()
  private lazy val playersIdsList = players.map(_.id)

  var galaxy = GalaxyGenerator(playersIdsList, scala.util.Random.nextInt(10))
  var roundNo = 0
  var activePlayersList = playersIdsList
  lazy val arenaTimeout = config.get[Duration]("app.arena.roundTimeout", 10 seconds)

  var resultSent = false

  override def receive: Receive = LoggingReceive {
    case Start => startGame
    case GetStatus =>
      sender ! CurrentStatus(false)
  }

  def startGame = {
    log(s"Game with id [$id] started. Players: [${playersIdsList.mkString(" - ")}]", INFO)
    players foreach { pl => pl.aRef ! BeReady(id, playersIdsList diff List(pl.id)) }

    evoluteGalaxy

    daoWorker ! SetInitialGalaxy(id, produceJS(galaxy))
    broadcastGalaxy

    context setReceiveTimeout arenaTimeout
    context become activeGameContext
  }

  def activeGameContext: Actor.Receive = LoggingReceive {
    case action: PlayerAction if activePlayersList.contains(action.id) && action.roundNo == roundNo =>
      roundAction.put(action.id, action.action)
    case GetStatus => sender ! CurrentStatus(true)
    case ReceiveTimeout => nextRound
  }

  def nextRound = {
    revoluteGalaxy
    evoluteGalaxy

    daoWorker ! AddLogRecord(id, roundNo, produceJS(arenaHistory(roundNo)), produceJS(galaxy))

    informPlayersAboutStatusChanges

    if (roundNo >= 50 || winnerExists(galaxy) || activePlayersList.isEmpty) finishGame
    else broadcastGalaxy
  }

  def revoluteGalaxy {
    val (nGalaxy, moves) = gp.revolution(galaxy, roundAction.values.toList)
    galaxy = nGalaxy
    arenaHistory.put(roundNo, moves)
    roundAction.clear()
  }

  def evoluteGalaxy = { galaxy = gp.evolution(galaxy) }

  def broadcastGalaxy: Unit = {
    roundNo += 1
    players foreach { pl => pl.aRef ! NextTurn(id, roundNo, produceJS(galaxy))}
    log(s"Round [$roundNo] started in Game with id [$id]", INFO)
  }

  def winnerExists(galaxy: Galaxy) =
    activePlayersList.exists(pId => galaxy.planets.forall(pl => pl.owner == Some(pId)))

  def finishGame = {
    val pls = galaxy.players.toList.sortBy { - _._2}
    val winners = pls.headOption.toSeq
    val loosers = pls.drop(1)

    winners foreach { case (pId, us) => players.find(_.id == pId).foreach( pl => pl.aRef ! YouWin(us)) }
    loosers foreach { case (pId, us) => players.find(_.id == pId).foreach( pl => pl.aRef ! YouLose(us)) }

    val gr = formGameResult
    players.map(_.aRef).foreach {_ ! gr}
    sendResultToLadder(gr)

    log(s"Game [$id] finished. Result [${produceJS(galaxy.players)}]", INFO)
    context.stop(self)
  }

  def informPlayersAboutStatusChanges = {
    val apMap = galaxy.players.filter(p => activePlayersList.contains(p._1))
    apMap foreach {
      case (pId, us) if us <= 0 =>
        players.find(_.id == pId).map(_.aRef).toSeq.foreach(p => p ! YouLose(0))
        activePlayersList = activePlayersList diff List(pId)
      case _ => ()
    }
  }

  def sendResultToLadder(gr: GameResult) = {
    log(s"result of game [$id] was sent to ladder", INFO)
    resultSent = true
    ladder ! SetGameResult(gr)
  }

  //def createRounds = arenaHistory.toList.sortBy(_._1) map {case (no, ms) => Round(no, ms)}

  def formGameResult: GameResult = GameResult(
    gameId = id,
    galaxy = produceJS(galaxy),
    playersResult = galaxy.players,
    rounds = roundNo,
    winner = gp.winnerId(galaxy.players)
  )

  @scala.throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    daoWorker ! Stop
    if(!resultSent) sendResultToLadder(formGameResult)
  }
}

case object Start

case object GetStatus

case class CurrentStatus(started: Boolean)