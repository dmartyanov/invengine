package ru.tcsbank.engine.ladder

import akka.actor.ActorRef
import ru.tcsbank.bot.BotMove

/**
 * Created by d.a.martyanov on 09.12.14.
 */
case class Player(
                   id: String,
                   ticket: String,
                   aRef: ActorRef
                   )

case class Game(
                 id: String,
                 players: List[Player],
                 arena: ActorRef
                 )

case class Round(
                  no: Int,
                  moves: List[BotMove]
                  )


