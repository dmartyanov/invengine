package ru.tcsbank.engine.ladder

/**
 * Created by d.a.martyanov on 25.12.14.
 */
trait EloRatingCalculus {

  val base = 50
  val k = 8

  private def elo = (diff: Int) => 1 / (1 + Math.pow( (base / k), (diff.toDouble / (base * k)) ) )
  private def probability = (diff: Int) => 1 - elo(diff)

  def diffRating = (diff: Int) => (base * probability(diff)).toInt
}
