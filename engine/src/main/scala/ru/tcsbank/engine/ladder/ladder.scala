package ru.tcsbank.engine.ladder

import akka.actor.{Actor, ActorRef, Props, ReceiveTimeout}
import akka.event.LoggingReceive
import ru.tcsbank.engine.arena.{ArenaManager, Start}
import ru.tcsbank.utils.config.ConfigHelper
import ru.tcsbank.connection.GameResult
import ru.tcsbank.dao.reactive._
import ru.tcsbank.utils.json.JsonHelper
import ru.tcsbank.system.Global.ConfigurationImpl
import ru.tcsbank.system.Logger
import ru.tcsbank.utils.logging.LoggerHelper

import scala.concurrent.duration._

/**
 * Created by d.a.martyanov on 09.12.14.
 */
class LadderManager extends Actor with ArenaIDGenerator {
  this: LoggerHelper with ConfigHelper =>

  val lf = Logger[LadderManager]
  lazy val minPlayersNo = config.get[Int]("app.ladder.minPlayers", 2)
  lazy val maxPlayersNo = config.get[Int]("app.ladder.maxPlayers", 4)
  val waitingPlayers = scala.collection.mutable.Map[String, Player]()
  val playersInGame = scala.collection.mutable.Map[String, Player]()
  val currentGames = scala.collection.mutable.Map[String, Game]()

  val engine = context.parent
  val ladderDao = context.actorOf(Props(new LadderDaoWorker with LoggerHelper with JsonHelper))
  lazy val ladderTimeout = config.get[Duration]("app.ladder.timeout", 10 seconds)

  context setReceiveTimeout ladderTimeout

  override def receive: Receive = LoggingReceive {
    case FindLadderGame(id, ticket, player) if !waitingPlayers.isDefinedAt(id) && !playersInGame.isDefinedAt(id) =>
      waitingPlayers.put(id, Player(id, ticket, player))
    case SetGameResult(gr) =>
      processGameResult(gr)
      ladderDao ! FinalyseGame(gr.gameId)
    case ReceiveTimeout if waitingPlayers.size >= minPlayersNo => startGame
  }

  def startGame: Unit = {
    val selectedTuples = waitingPlayers.take(Math.min(waitingPlayers.size, maxPlayersNo)).toList
    val activePlayers = selectedTuples map { case (id, pl) => pl}
    val arenaId = newArenaID
    val arena = context.actorOf(
      Props(new ArenaManager(arenaId, activePlayers, context.self)
        with LoggerHelper with ConfigurationImpl with JsonHelper)
    )

    currentGames.put(arenaId, Game(arenaId, activePlayers, arena))
    selectedTuples foreach {
      case (id, pl) =>
        waitingPlayers.remove(id)
        playersInGame.put(id, pl)
    }

    ladderDao ! AddGame(arenaId, activePlayers)
    arena ! Start
  }

  def processGameResult(gr: GameResult) = {
    ladderDao ! AddGameResult(gr)
    gr.playersResult.keys foreach playersInGame.remove
    ladderDao ! UpdatePlayerRating(gr.winner, gr.playersResult.keys.filterNot(_ == gr.winner).toList)
    currentGames.remove(gr.gameId)
    log(s"Game [${gr.gameId}] finished with results: [${gr.playersResult.mkString(" | ")}]", INFO)
  }

//  def processGameHistory(gameId: String, initialGalaxy: String, history: String) =
//    ladderDao ! AddGameLog(gameId, initialGalaxy, history)

  @scala.throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    lf.info(s"Ladder manager started on address ${context.self.path.toStringWithoutAddress}")
  }
}

case class FindLadderGame(id: String, ticket: String, player: ActorRef)

case class FindTestGame(id: String, ticket: String, player: ActorRef, difficulty: Int)

case class SetGameResult(gameResult: GameResult)
