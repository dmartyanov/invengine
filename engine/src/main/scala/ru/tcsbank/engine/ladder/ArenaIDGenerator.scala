package ru.tcsbank.engine.ladder

import ru.tcsbank.system.IdGenerator

/**
 * Created by d.a.martyanov on 09.12.14.
 */
trait ArenaIDGenerator extends IdGenerator {
  override val constantPrefix = "binvar"

  def newArenaID = prefixedId
}
