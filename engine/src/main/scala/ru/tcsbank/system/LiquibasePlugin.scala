package ru.tcsbank.system

import java.sql.Connection

import liquibase.Liquibase
import liquibase.changelog.ChangeSet
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.{ClassLoaderResourceAccessor, CompositeResourceAccessor, FileSystemResourceAccessor}
import ru.tcsbank.system.exception.DBAccessException
import ru.tcsbank.utils.logging.LoggerHelper

import scala.collection.JavaConversions._
import scala.util.{Failure, Success, Try}

/**
 * Created by d.a.martyanov on 18.12.14.
 */
class LiquibasePlugin(context: String) {
  this: LoggerHelper =>

  override val lf = Logger[LiquibasePlugin]

  private def getScriptDescriptions(changeSets: Seq[ChangeSet]) = {
    changeSets.zipWithIndex.map {
      case (cl, num) =>
        "" + num + ". " + cl.getId +
          Option(cl.getDescription).map(" (" + _ + ")").getOrElse("") +
          " by " + cl.getAuthor
    }.mkString("\n")
  }

  def start(connection: Connection, dbPath: String) = {
    log("Running migrations", INFO)
    Try(getLiquibase(connection)) map { liqui =>
      val liquiMigrations = liqui.listUnrunChangeSets(context)
      Try(liqui.update(context)) match {
        case Success(_) =>
          log(s"Migrations were completed successfully: \n${getScriptDescriptions(liquiMigrations)}\n" +
            s" on database [$dbPath]", INFO)
        case Failure(err) =>
          throw new RuntimeException(s"Couldn't apply migrations: [${getScriptDescriptions(liquiMigrations)}]" +
            s" on database [$dbPath]; error:  [${err.getMessage}}]")
      }
    } recover {
      case err: Exception =>
        loggedException(
          new DBAccessException(
            s"Migrations couldn't ve applied on context [$context] with error [${err.getMessage}]"
          ), ERROR
        )
    }
  }

  def getLiquibase(connection: Connection) = {
    ///Users/d.a.martyanov/projects/tcs/tcs/binv/enginepro/engine/src/main/resources/migrations
    val fileAccessor = new FileSystemResourceAccessor(
      "/Users/d.a.martyanov/projects/tcs/tcs/binv/enginepro/engine/src/main/resources/migrations"
    )
    val classLoaderAccessor = new ClassLoaderResourceAccessor(this.getClass.getClassLoader)
    val fileOpener = new CompositeResourceAccessor(fileAccessor, classLoaderAccessor)
    new Liquibase("changelog-master.xml", fileOpener, new JdbcConnection(connection))
  }
}
