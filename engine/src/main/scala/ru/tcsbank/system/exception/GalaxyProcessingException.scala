package ru.tcsbank.system.exception

/**
 * Created by d.a.martyanov on 12.12.14.
 */
class GalaxyProcessingException(msg: String, cauze: Option[Throwable] = None, descr: Option[String] = None)
  extends EngineException(msg, cauze, descr) {

  override def title: String = UsefulExceptionRegistry.GALAXY_EXCEPTION
}
