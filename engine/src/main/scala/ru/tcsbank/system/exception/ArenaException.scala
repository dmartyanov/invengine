package ru.tcsbank.system.exception

/**
 * Created by d.a.martyanov on 11.12.14.
 */
class ArenaException(msg: String, cauze: Option[Throwable] = None, descr: Option[String] = None)
  extends EngineException(msg, cauze, descr) {

  override def title: String = UsefulExceptionRegistry.ARENA_EXCEPTION
}
