package ru.tcsbank.system.exception


/**
 * Created by d.a.martyanov on 08.12.14.
 */
class IllegalRequestException(msg: String, cauze: Option[Throwable] = None, descr: Option[String] = None)
  extends EngineException(msg, cauze, descr) {

  override def title: String = UsefulExceptionRegistry.CLIENT_REQUEST_EXCEPTION
}
