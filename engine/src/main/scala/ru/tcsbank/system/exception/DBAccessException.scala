package ru.tcsbank.system.exception

/**
 * Created by d.a.martyanov on 18.12.14.
 */
class DBAccessException(msg: String, cauze: Option[Throwable] = None, descr: Option[String] = None)
  extends EngineException(msg, cauze, descr) {
  override def title: String = UsefulExceptionRegistry.DB_ACCESS_EXCEPTION
}
