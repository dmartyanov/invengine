package ru.tcsbank.system

import java.util.concurrent.{LinkedBlockingDeque, TimeUnit, ThreadPoolExecutor}

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import ru.tcsbank.utils.as.ActorSystemHelper
import ru.tcsbank.utils.config.ConfigHelper
import ru.tcsbank.dao.conn.{TransactionDBManager, MySQLDatabaseComponent, DatabaseHelper, MySQLDatabaseManager}
import ru.tcsbank.engine.ladder.LadderManager
import ru.tcsbank.utils.context.ExecutionContextComponent
import ru.tcsbank.utils.logging.LoggerHelper

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

/**
 * Created by d.a.martyanov on 08.12.14.
 */
object Global extends LazyLogging{

  private lazy val configImpl = ConfigFactory.load()
  trait ConfigurationImpl extends ConfigHelper{
    override lazy val config = configImpl
  }

  private lazy val actors = ActorSystem("binvengine", configImpl)
  trait ActorSystemComponentImpl extends ActorSystemHelper{
    override implicit def actorSystem: ActorSystem = actors
  }
  trait DefaultExecutionContextComponentImpl extends ExecutionContextComponent {
    override implicit def executionContext: ExecutionContext = actors.dispatcher
  }

  lazy val ladderManager = new LadderManager with LoggerHelper with ConfigurationImpl

  lazy val pooledDB = new MySQLDatabaseManager with LoggerHelper with ConfigurationImpl
  trait MySQLDatabase extends DatabaseHelper {
    override val db: MySQLDatabaseComponent = pooledDB
  }

  lazy val liquiProductionMigrator = new LiquibasePlugin("prod") with LoggerHelper

  lazy val dataBaseRequestExecutionPool = ExecutionContext.fromExecutorService(
    new ThreadPoolExecutor(
      Runtime.getRuntime.availableProcessors(),
      16, (3 minute) toMillis,
      TimeUnit.MILLISECONDS,
      new LinkedBlockingDeque[Runnable]()
    )
  )
  trait DatabaseExecutionContext extends ExecutionContextComponent{
    override implicit def executionContext: ExecutionContext = dataBaseRequestExecutionPool
  }

  lazy val mySQLTransactionManager = new TransactionDBManager with DatabaseExecutionContext with MySQLDatabase
}
