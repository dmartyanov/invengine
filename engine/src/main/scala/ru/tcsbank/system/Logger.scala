package ru.tcsbank.system

import org.slf4j.LoggerFactory

import scala.reflect.ClassTag

/**
 * Created by d.a.martyanov on 08.12.14.
 */
object Logger{
  def apply[T](implicit cls: ClassTag[T]) = com.typesafe.scalalogging.Logger(LoggerFactory.getLogger(cls.runtimeClass))
}
