package ru.tcsbank.system

import java.util.UUID

/**
 * Created by d.a.martyanov on 23.12.14.
 */
trait IdGenerator {
  val constantPrefix: String

  def prefixedId = customPrefixId(constantPrefix)

  def customPrefixId(prefix: String) = prefix + UUID.randomUUID().toString.replace("-", "")
}
