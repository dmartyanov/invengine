package ru.tcsbank.system

import akka.actor.Props
import com.typesafe.scalalogging.StrictLogging
import ru.tcsbank.connection.EngineConnectionManager
import ru.tcsbank.utils.json.JsonHelper
import ru.tcsbank.system.Global.{DefaultExecutionContextComponentImpl, MySQLDatabase, ConfigurationImpl, ActorSystemComponentImpl}
import ru.tcsbank.utils.logging.LoggerHelper

/**
 * Created by d.a.martyanov on 08.12.14.
 */
object Main extends App with ActorSystemComponentImpl with StrictLogging with MySQLDatabase{
  logger.info("Boot BINV engine")

  Global.liquiProductionMigrator.start(db.connection, db.url)

  actorSystem.actorOf(
    Props(new EngineConnectionManager with ConfigurationImpl
      with LoggerHelper with JsonHelper with DefaultExecutionContextComponentImpl),
    "engine"
  )
}
