package ru.tcsbank.dao.reactive

import akka.actor.{Actor, ActorLogging}
import akka.event.LoggingReceive
import ru.tcsbank.connection.ClientConnection
import ru.tcsbank.dao.model.{Connection, User}
import ru.tcsbank.dao.schema.{ConnectionsDao, UserDao}
import ru.tcsbank.system.Logger
import ru.tcsbank.utils.logging.LoggerHelper

/**
 * Created by d.a.martyanov on 23.12.14.
 */
class EngineDaoWorker extends Actor with ActorLogging with SafeOperationProvider {
  this: LoggerHelper =>

  override val lf = Logger[EngineDaoWorker]

  override def receive: Receive = LoggingReceive {
    case AppendUser(u) => operationSafeWithLogging(UserDao.append(u), "Appending user", ())
    case AddConnection(c) => operationSafeWithLogging(
      ConnectionsDao.insert(
        Connection(
          ticket = c.ticket,
          userId = c.id,
          closed = 0,
          creationDate = c.dateTime,
          address = c.addrr
        )
      ), "Addind new connection", ())
    case CloseConnection(t) => operationSafeWithLogging(ConnectionsDao.close(t), "Closing connection", ())
  }

}

case class AppendUser(u: User)

case class AddConnection(c: ClientConnection)

case class CloseConnection(ticket: String)
