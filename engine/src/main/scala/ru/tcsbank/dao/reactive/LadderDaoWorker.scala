package ru.tcsbank.dao.reactive

import akka.actor.{Actor, ActorLogging}
import akka.event.LoggingReceive
import ru.tcsbank.dao.model._
import ru.tcsbank.dao.schema._
import ru.tcsbank.utils.json.JsonHelper
import ru.tcsbank.engine.ladder.{EloRatingCalculus, Player}
import ru.tcsbank.system.Logger
import ru.tcsbank.system.exception.DBAccessException
import ru.tcsbank.utils.logging.LoggerHelper

/**
 * Created by d.a.martyanov on 23.12.14.
 */
class LadderDaoWorker extends Actor with ActorLogging with EloRatingCalculus
with SafeOperationProvider {
  this: LoggerHelper with JsonHelper =>

  import ru.tcsbank.dao.model.GameResultIdGenerator._

  override val lf = Logger[LadderDaoWorker]

  override def receive: Receive = LoggingReceive {
    case AddGame(gId, players) =>
      operationSafeWithLogging(
        GameDao.insert(
          Game(
            gameId = gId,
            creationDate = new java.util.Date().getTime,
            finished = 0,
            rounds = 0
          )
        ), "Game submitting", ()
      )
      players foreach { pl =>
        operationSafeWithLogging(
          GamePlayersDao.insert(
            PlayerGameRelation(
              gameId = gId,
              userId = pl.id,
              ticket = pl.ticket
            )
          ), "Appending game-player relation", ()
        )
      }
    case AddGameResult(gr) =>
      operationSafeWithLogging({
        GameDao.find(gr.gameId) match {
          case Some(game) if game.finished == 0 =>
            val resultId = newGameResultId
            GameResultDao.insert(
              GameResult(
                resultId = resultId,
                galaxy = gr.galaxy,
                playersResult = produceJS(gr.playersResult),
                rounds = gr.rounds,
                winnerId = gr.winner
              )
            )
            GameDao.update(game.copy(gameResultId = Some(resultId), rounds = gr.rounds))
          case _ => throw new DBAccessException(s"Game with id [${gr.gameId}] is not accessible")
        }
      }, s"Appenging game result", ())

    case FinalyseGame(gameId) =>
      operationSafeWithLogging({
        GameDao.find(gameId) match {
          case Some(game) if game.finished == 0 =>
            GameDao update game.copy(finished = 1)
            GameLogDao updateLastTurn (gameId, game.rounds)
          case _ => throw new DBAccessException(s"Game with id [${gameId}] is not accessible")
        }
      }, s"Finalizing game", ())

    case UpdatePlayerRating(w, ls) if !ls.isEmpty =>
      operationSafeWithLogging({
        val winner  = UserDao.find(w).get
        val loosers = UserDao.filter(ls)
        var winnerBonus = 0
        loosers foreach { looser =>
          val rateDiff = diffRating(looser.rating - winner.rating)
          UserDao.update(looser.copy(rating = looser.rating - rateDiff))
          winnerBonus += rateDiff
        }
        if(winnerBonus != 0) UserDao.update(winner.copy(rating = winner.rating + winnerBonus))

      }, s"Updating players rating for [$w] and [${ls.mkString(" ")}]", ())
  }
}

case class AddGame(gameId: String, players: List[Player])

case class AddGameResult(gr: ru.tcsbank.connection.GameResult)

case class UpdatePlayerRating(winner: String, loosers: List[String])

case class FinalyseGame(gameId: String)