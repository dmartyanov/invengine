package ru.tcsbank.dao.reactive

import ru.tcsbank.utils.logging.LoggerHelper

import scala.util.{Failure, Success, Try}

/**
 * Created by d.a.martyanov on 23.12.14.
 */
trait SafeOperationProvider {
  this: LoggerHelper =>

  def operationSafeWithLogging[T](op: => T, description: String, default: T): T = Try(op) match {
    case Success(res) => res
    case Failure(err) =>
      log(s"Operation [$description] failed background with error [${err.getMessage}]", ERROR)
      default
  }
}
