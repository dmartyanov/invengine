package ru.tcsbank.dao.reactive

import akka.actor.{Actor, ActorLogging}
import ru.tcsbank.dao.model.GameLog
import ru.tcsbank.dao.schema.{GameLogDao, GameDao}
import ru.tcsbank.system.Logger
import ru.tcsbank.system.exception.DBAccessException
import ru.tcsbank.utils.logging.LoggerHelper

/**
 * Created by d.a.martyanov on 24.12.14.
 */
class ArenaDaoWorker extends Actor with ActorLogging
with SafeOperationProvider {
  this: LoggerHelper =>

  override val lf = Logger[ArenaDaoWorker]

  override def receive: Receive = {
    case SetInitialGalaxy(gameId, galaxy) =>
      operationSafeWithLogging({
        GameDao.find(gameId) match {
          case Some(game) if game.finished == 0 =>
            GameDao.update(game.copy(initialGalaxy = Some(galaxy)))
          case _ => throw new DBAccessException(s"Game [$gameId] is not accessible")
        }
      }, "Appending initial galaxy to game", ())

    case AddLogRecord(gId, round, moves, rGal) =>
      operationSafeWithLogging({
        GameDao.find(gId) match {
          case Some(game) if game.finished == 0 =>
            GameLogDao.insert(GameLog(
              gameId = game.gameId,
              round = round,
              moves = moves,
              galaxy = rGal,
              last = 0
            ))
          case _ => throw new DBAccessException(s"Game [$gId] is not accessible")
        }
      }, "Appending log record to gamelog", ())

    case Stop => context.stop(self)
  }
}

case class SetInitialGalaxy(gameId: String, galaxy: String)

case class AddLogRecord(gameId: String, round: Int, moves: String, resultGalaxy: String)

case object Stop
