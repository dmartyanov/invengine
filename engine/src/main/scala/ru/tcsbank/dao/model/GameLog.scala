package ru.tcsbank.dao.model

import ru.tcsbank.system.IdGenerator

import scala.slick.lifted.ProvenShape
import slick.driver.MySQLDriver.simple._

/**
 * Created by d.a.martyanov on 22.12.14.
 */
case class GameLog(
                    gameId: String,
                    round :Int,
                    moves: String,
                    galaxy: String,
                    last: Int
                    )

class GameLogs(tag: Tag) extends Table[GameLog](tag, "gamelog"){
  def gameId = column[String]("game_id", O.NotNull)

  def round = column[Int]("round", O.NotNull)

  def moves = column[String]("moves", O.NotNull)

  def galaxy = column[String]("resultgalaxy", O.NotNull)

  def last = column[Int]("last", O.NotNull)

  override def * : ProvenShape[GameLog] = (gameId, round, moves, galaxy, last) <> (GameLog.tupled, GameLog.unapply)

  def game = foreignKey("fk_gamelog_game_id", gameId, TableQuery[Games])(_.gameId)
}

trait GameLogIdGenerator extends IdGenerator{
  override val constantPrefix = "gamlog"
  def newGameLogId = prefixedId
}

object GameLogIdGenerator extends GameLogIdGenerator