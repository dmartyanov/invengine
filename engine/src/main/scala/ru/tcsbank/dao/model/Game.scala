package ru.tcsbank.dao.model

import scala.slick.driver.MySQLDriver.simple._
import scala.slick.lifted.ProvenShape

/**
 * Created by d.a.martyanov on 22.12.14.
 */
case class Game(
                 gameId: String,
                 creationDate: Long,
                 finished: Int,
                 gameResultId: Option[String] = None,
                 initialGalaxy: Option[String] = None,
                 rounds: Int
                 )

class Games(tag: Tag) extends Table[Game](tag, "game") {

  def gameId = column[String]("game_id", O.PrimaryKey)

  def creationDate = column[Long]("creation_date", O.NotNull)

  def finished = column[Int]("finished", O.NotNull)

  def gameResultId = column[String]("game_result_id")

  def initialGalaxy = column[String]("initialgalaxy")

  def rounds = column[Int]("rounds", O.NotNull)

  override def * : ProvenShape[Game] =
    (gameId, creationDate, finished, gameResultId.?, initialGalaxy.?, rounds) <>(Game.tupled, Game.unapply)

  def gameResult = foreignKey("fk_game_gameresult_id", gameResultId, TableQuery[GameResults])(_.resultId)
}
