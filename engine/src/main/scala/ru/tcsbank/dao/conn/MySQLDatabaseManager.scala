package ru.tcsbank.dao.conn

import java.sql.Connection

import com.mchange.v2.c3p0.DataSources
import ru.tcsbank.utils.config.ConfigHelper
import ru.tcsbank.system.Logger
import ru.tcsbank.system.exception.DBAccessException
import ru.tcsbank.utils.logging.LoggerHelper
import slick.driver.MySQLDriver.simple._


import scala.util.{Failure, Success, Try}

/**
 * Created by d.a.martyanov on 18.12.14.
 */
class MySQLDatabaseManager extends MySQLDatabaseComponent{
  this: ConfigHelper with LoggerHelper =>

  override val lf = Logger[MySQLDatabaseManager]

  lazy val host = config.get[String]("db.host", "localhost")
  lazy val port = config.get[Int]("db.port", 3306)
  lazy val db = config.get[String]("db.database", "binv")

  lazy val url = s"""jdbc:mysql://$host:$port/$db"""

  lazy val user = config.get[String]("db.user")
  lazy val pwd = config.get[String]("db.pwd")

  lazy val maxPoolSize = config.get[Int]("db.maxPoolSize", 16)

  lazy val dsPooled = {
    val unpool = DataSources.unpooledDataSource(url, user, pwd)
    val props = new java.util.HashMap[String, Any]()
    props.put("maxPoolSize", maxPoolSize)
    props.put("minPoolSize", new Integer(2))
    props.put("AcquireIncrement", new Integer(2))
    props.put("driver-class", "com.mysql.jdbc.Driver")
    DataSources.pooledDataSource(unpool, props)
  }

  lazy val dbFromDs = Database.forDataSource(dsPooled)

  def inTransaction[T](l: Session => T) = dbFromDs.withTransaction { implicit session =>
    Try(l(session)) match {
      case Success(result) => result
      case Failure(err) =>
        loggedException(
          new DBAccessException(s"DB operation failed - error: [${err.getMessage}]", Some(err)),
          ERROR, true)
    }
  }

  def connection: Connection = dbFromDs.createConnection()
}
