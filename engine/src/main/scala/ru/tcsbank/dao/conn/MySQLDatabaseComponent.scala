package ru.tcsbank.dao.conn

import java.sql.Connection

import scala.slick.driver.MySQLDriver.simple._

/**
 * Created by d.a.martyanov on 19.12.14.
 */
trait MySQLDatabaseComponent {

  val url: String

  def connection: Connection

  def inTransaction[T](l: Session => T): T
}
