package ru.tcsbank.dao.conn

import ru.tcsbank.utils.context.ExecutionContextComponent

import scala.concurrent.Future
import scala.slick.driver.MySQLDriver.simple._

/**
 * Created by d.a.martyanov on 22.12.14.
 */
trait TransactionDBManager {
  this: ExecutionContextComponent with DatabaseHelper =>

  def tx[T](l: Session => T): T = db.inTransaction[T](l)

  def atx[T](l: Session => T): Future[T] = Future {
    db.inTransaction[T](l)
  }
}
