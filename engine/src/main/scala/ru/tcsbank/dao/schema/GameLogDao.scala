package ru.tcsbank.dao.schema

import ru.tcsbank.dao.model.{GameLog, GameLogs}

import scala.slick.driver.MySQLDriver.simple._
/**
 * Created by d.a.martyanov on 23.12.14.
 */
object GameLogDao {

  import ru.tcsbank.system.Global.mySQLTransactionManager._

  val logs = TableQuery[GameLogs]

  def list = tx {implicit session => logs.list }

  def insert(log: GameLog) = tx { implicit session => logs += log }

  def findRecord(gId: String, round: Int) = tx { implicit session =>
    logs.list.filter(l => l.gameId == gId && l.round == round).headOption
  }

  def updateLastTurn(gameId: String, last: Int) = tx { implicit session =>
    val lastTurn = logs.list.filter(_.gameId == gameId).filter(_.round == last).head
    logs.filter(_.gameId === gameId).filter(_.round === last).update(lastTurn.copy(last = 1))
  }
}
