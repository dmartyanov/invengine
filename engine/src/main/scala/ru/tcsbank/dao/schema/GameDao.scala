package ru.tcsbank.dao.schema

import ru.tcsbank.dao.model.{Game, Games}
import scala.slick.driver.MySQLDriver.simple._

/**
 * Created by d.a.martyanov on 23.12.14.
 */
object GameDao {

  import ru.tcsbank.system.Global.mySQLTransactionManager._

  val games = TableQuery[Games]

  def list = tx {implicit session => games.list }

  def find(gId: String) = tx { implicit session =>
    games.filter(_.gameId === gId).list.headOption
  }

  def insert(g: Game) = tx {implicit session => games += g }

  def update(g: Game) = tx {implicit session =>
    games.filter(_.gameId === g.gameId).update(g)
  }

}
