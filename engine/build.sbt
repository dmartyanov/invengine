name := "binvengine"

version := "1.0"

scalaVersion := "2.11.4"

organization := "www.tcsbank.ru"

resolvers ++= Seq(
  "Maven Local"         at Path.userHome.toURI + ".m2/repository",
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Typesafe Releases"   at "http://repo.typesafe.com/typesafe/maven-releases/",
  "TCS Snapshots"       at "http://nexus.tcsbank.ru/content/repositories/tcs-snapshot",
  "TCS Maven Central"   at "http://nexus.tcsbank.ru/content/repositories/central",
  "TCS Typesafe"        at "http://nexus.tcsbank.ru/content/repositories/typesafe",
  "TCS Releases"        at "http://nexus.tcsbank.ru/content/repositories/tcs",
  "TCS Google Code"     at "http://nexus.tcsbank.ru/content/repositories/google"
)

libraryDependencies ++=
    akka("2.3.4") ++
    db ++
  Seq(
    "org.scala-lang"              % "scala-reflect"         % "2.11.4",
    "com.typesafe.scala-logging"  %% "scala-logging"        % "3.1.0",
    "org.json4s"                  %%  "json4s-jackson"      % "3.2.10",
    "ch.qos.logback"              %   "logback-classic"     % "1.1.2",
    "org.scalatest"               %%  "scalatest"           % "2.2.0"     % "test",
    "org.scalamock"               %% "scalamock-scalatest-support"    % "3.2.1"     % "test"
  )

def akka(v: String) = Seq(
  "com.typesafe.akka" %% "akka-actor"   % v,
  "com.typesafe.akka" %% "akka-remote"  % v,
  "com.typesafe.akka" %% "akka-slf4j"   % v,
  "com.typesafe.akka" %% "akka-testkit" % v % "test"
)

def db = Seq(
  "com.mchange"             %   "c3p0"                    % "0.9.2.1",
  "com.typesafe.slick"      %%  "slick"                   % "2.1.0",
  "org.liquibase"           % "liquibase-core"            % "3.2.2",
  "mysql"                   % "mysql-connector-java"      % "5.1.34"
)